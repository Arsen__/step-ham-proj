function selectPanel (e){
    let target = e.target.dataset.target;
    document.querySelectorAll('.filter-tabs-item').forEach(el =>(el.classList.remove('active')));
    e.target.classList.add('active');
    const selector = this.dataset.filter;
    const portfolio = document.querySelectorAll('.item');
    portfolio.forEach(item => item.style.display = 'none');
    const portfolioItem = document.querySelectorAll(`${selector}.item`);
    portfolioItem.forEach(elem => elem.style.display = 'block');
}
document.querySelectorAll('.filter-tabs-item').forEach(el =>{
    el.addEventListener('click', selectPanel)
});


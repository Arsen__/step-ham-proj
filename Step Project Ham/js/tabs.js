document.querySelector('.menu-item').classList.add('active');
document.querySelector('.menu-block').classList.add('active');
function selectPanel (e){
    let target = e.target.dataset.target;
    document.querySelectorAll('.menu-item, .menu-block').forEach(el =>(el.classList.remove('active')));
    e.target.classList.add('active');
    document.querySelector('.' + target).classList.add('active');
}
document.querySelectorAll('.menu-item').forEach(el =>{
    el.addEventListener('click', selectPanel)
});
function seeMore() {
    let dots = document.getElementById("nodisp");
    let dots2 = document.getElementById("nodisp2");
    let dots3 = document.getElementById("nodisp3");
    let deleteBtn = document.getElementById("btn");
    if (dots.style.display === "none") {
        dots.style.display = "inline";
        dots2.style.display = "inline";
        dots3.style.display = "inline";
        dots3.style.marginBottom = "150px";
        deleteBtn.style.display = "none";
    } else {
        dots.style.display = "none";
        dots2.style.display = "none";
        dots3.style.display = "none";
        deleteBtn.style.display = "inline";
    }
}